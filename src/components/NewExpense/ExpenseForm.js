import React from 'react';
import { useState } from 'react';
import './ExpenseForm.css';


const ExpenseForm = (props) => {
    const [title,setNewTitle] = useState('');
    const [amount, setNewAmount] = useState('');
    const [enteredDate, setNewEnteredDate] = useState('');

    const titleChangeHandler = (event) => {
        setNewTitle(event.target.value)
    }

    const amountChangeHandler = (event) => {
        setNewAmount(event.target.value)
    }

    const dateChangeHandler = (event) => {
        setNewEnteredDate(event.target.value)
    }

    // const inputChangeHandler = (identifier, value) => {
    //     if (identifier === 'title'){ 
    //         setNewTitle(value);
    //     } else if (identifier === 'amount'){
    //         setNewAmount(value);
    //     } else {
    //         setNewEnteredDate(value);
    //     }
    // }
    
    const onSubmitHandler = (event) => {
        event.preventDefault();
        const expenseData = {
            title: title,
            amount: amount,
            date: new Date(enteredDate)
        };
        props.onSaveExpenseData(expenseData);
        setNewTitle('');
        setNewAmount('');
        setNewEnteredDate('');
    }


    return (
        <form onSubmit={onSubmitHandler}> 
            <div className='new-expense__controls'>
                <div className='new-expense__control'>
                    <label>Title</label>
                    <input type='text' value={title} onChange={titleChangeHandler} />
                </div>
                <div className='new-expense__control'>
                    <label>Amount</label>
                    <input type='text' min='0.01' step='0.01' value={amount} onChange={amountChangeHandler} />
                </div>
                <div className='new-expense__control'>
                    <label>Date</label>
                    <input type='date' min='2019-01-01' max='2022-12-31' value={enteredDate} onChange={dateChangeHandler} />
                </div>
                <div className="new-expense__actions">
                    <button type="button" onClick={props.onCancel}>Cancel</button>
                    <button type="submit">Add Expense</button>
                </div>
            </div>
        </form>
    )
}

export default ExpenseForm;